//
//  InterfaceController.swift
//  activity3 WatchKit Extension
//
//  Created by Satinder pal Singh on 2019-10-31.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
let countriesList:[String] = ["America/Toronto", "America/Vancouver", "Europe/London","Asia/Kolkata"]
    @IBOutlet weak var outputLabel: WKInterfaceLabel!
    @IBOutlet weak var table: WKInterfaceTable!
    
    @IBAction func buttonPressed() {
        
        AF.request("https://worldtimeapi.org/api/timezone/Asia/Kolkata").responseJSON {
            
            (xyz) in
            print(xyz.value)
            
            // convert the response to a JSON object
            let x = JSON(xyz.value)
            let sunrise = x["results"]["sunrise"]
            let sunset = x["results"]["sunset"]
            
            print("Sunrise: \(sunrise)")
            print("Sunset: \(sunset)")
            self.outputLabel.setText("Sunrise: \(sunrise)")
        }
        
    }
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        if (WCSession.default.isReachable) {
            let message = ["countryName":rowIndex] as [String : Any]
            
            WCSession.default.sendMessage(message, replyHandler: nil)
        }

    }
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported() == true) {
            //nameLabel.setText("WC enabled")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("WC NOT supported!")
        }
        
        
        self.table.setNumberOfRows(self.countriesList.count, withRowType: "rowIdentifier")
        for (index, country) in self.countriesList.enumerated() {
            let row = self.table.rowController(at: index) as! ToDoRowController
            row.toDoLabel.setText(country)
        }
    }
    
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
