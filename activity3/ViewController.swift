//
//  ViewController.swift
//  activity3
//
//  Created by Satinder pal Singh on 2019-10-31.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import UIKit
import WatchConnectivity
import SwiftyJSON
import Alamofire
import Particle_SDK

class ViewController: UIViewController,WCSessionDelegate {
    let countriesList:[String] = ["America/Toronto", "America/Vancouver", "Europe/London","Asia/Kolkata"]
    let countriesListWeather:[String] = ["Toronto", "Vancouver", "London" , "Kolkata"]
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var outputLabel: UILabel!
    var forecasts: Array<String>!
    
    var countryName = ""
    
    
    var timeHours = ""
    var timeMinutes = ""
    var temperatureToday = ""
    var temperatureTomorrow = ""
    var weatherToday = ""
    
    var timeHoursAndMinutes = ""
    
    
    let USERNAME = "gurlaganbhullar@gmail.com"
    let PASSWORD = "12345"
    
    let DEVICE_ID = "420031000f47363333343437"
    var myPhoton : ParticleDevice?
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
   
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        
        let rowIndex = message["countryName"] as! Int
        
        let countryName = countriesList[rowIndex]
        self.countryName = countryName
        
        DispatchQueue.main.async {
            self.countryNameLabel.text = "\(self.countryName)"
        }
        
        let countryNameWeather = countriesListWeather[rowIndex]
        AF.request("https://worldtimeapi.org/api/timezone/\(countryName)").responseJSON {
            
            (xyz) in
            //print(xyz.value)
            
            // convert the response to a JSON object
            let x = JSON(xyz.value)
            let sunrise = x["datetime"]
            let timeString = sunrise.stringValue
            print("timeString: \(timeString)")
            
            
            
            //let sunset = x["results"]["sunset"]
            //Hours
            let rangeStartindex = timeString.index(timeString.startIndex, offsetBy: 11)
            let rangeEndIndex = timeString.index(timeString.endIndex, offsetBy: -20)
            let timeHour = timeString[rangeStartindex...rangeEndIndex]
            print(timeHour)
            
            
            //Minutes
            let mrangeStartindex = timeString.index(timeString.startIndex, offsetBy: 14)
            let mrangeEndIndex = timeString.index(timeString.endIndex, offsetBy: -17)
            let mtimeMinute = timeString[mrangeStartindex...mrangeEndIndex]
            print(mtimeMinute)
            
            self.timeHours = timeHour+""
            self.timeMinutes = mtimeMinute+""
            
            self.timeHoursAndMinutes = timeHour+mtimeMinute+""
            
            //print("Sunrise: \(sunrise)")
            //print("Sunset: \(sunset)")
            //            self.outputLabel.text = ("Sunrise: \(sunrise)")
            //            self.labelCountryName.text = "\(countryName)"
        }
        
        AF.request("https://api.openweathermap.org/data/2.5/weather?q=\(countryNameWeather)&units=metric&APPID=acebdb4548ced12a28df4ccf585c3466").responseJSON {
            
            (xyz) in
            // convert the response to a JSON object
            let x = JSON(xyz.value)
            // print(x)
            
            let sunrise = x["weather"].arrayValue
            let weatherString = sunrise[0]["main"].stringValue
            print(weatherString)
            self.weatherToday = weatherString
            
            
            
            let temp = x["main"]["temp"]
            //print(temp.stringValue)
            let tempString = temp.stringValue
            print(tempString)
            let tempDouble = Double(tempString)!
            let temprature = Int(tempDouble)
            print("temprature: \(temprature)")
            
            self.temperatureToday = "\(temprature)"
            
        }
        AF.request("https://api.openweathermap.org/data/2.5/forecast/daily?q=\(countryNameWeather)&mode=json&units=metric&cnt=2&APPID=acebdb4548ced12a28df4ccf585c3466").responseJSON {
            
            
            (xyz) in
            // convert the response to a JSON object
            let x = JSON(xyz.value)
            print(x)
            let temp = x["list"]
            print("tmrw: \(temp)")
            
            
            let forecastList = x["list"].array!
            self.forecasts = Array<String>()
            
            for json in forecastList
            {    self.forecasts.append(json["temp"]["day"].stringValue)
                print("hello hello")
                print(json["temp"]["day"].stringValue)
            }
            
            let tempStringTommorow = self.forecasts[1]
            print(tempStringTommorow)
            let tempDoubleTommorow = Double(tempStringTommorow)!
            let tempratureTommorow = Int(tempDoubleTommorow)
            print("temprature Tomorow: \(tempratureTommorow)")

            self.temperatureTomorrow = "\(tempratureTommorow)"
            
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if(WCSession.isSupported() == true){
            
            let session = WCSession.default
            session.delegate = self
            session.activate()
            print("phone supported the WC session")
        }
        else{
            print("wc session not supported in phone")
        }
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        }
    }
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                
            }
            
        } // end getDevice()
    }
    
    
    
    @IBAction func weatherButton(_ sender: Any) {
        
        if(self.weatherToday == "Clouds"){
            // turnCloud()
            
            chandeWeather(weather: "Grey")
        }
        else if(self.weatherToday == "Snow"){
            chandeWeather(weather: "White")
            //turnSnowy()
        }
        else if(self.weatherToday == "Sunny"){
            chandeWeather(weather: "Yellow")
            // turnSunny()
        }
        else if(self.weatherToday == "Clear"){
            chandeWeather(weather: "Yellow")
            // turnSunny()
        }
        else if(self.weatherToday == "Rain"){
            chandeWeather(weather: "Blue")
            // turnRainy()
        }
        else if(self.weatherToday == "Mist"){
            chandeWeather(weather: "Blue")
            // turnRainy()
        }
        else if(self.weatherToday == "Haze"){
            chandeWeather(weather: "Red")
            
        }
        else if(self.weatherToday == "Fog"){
            chandeWeather(weather: "Pink")
            
        }
        
    }
    
    @IBAction func temperatureButton(_ sender: Any) {
        showTemperature(temperature: self.temperatureToday)
        
    }
    
    @IBAction func temperatureTomorrow(_ sender: Any) {
        showTemperature(temperature: self.temperatureTomorrow)
    }
    @IBAction func timeButton(_ sender: Any) {
        
        self.showTime()
    }
    func showTemperature(temperature: String) {
        let parameters = ["\(temperature)"]
        var task = myPhoton!.callFunction("showTemperature", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle")
            }
            else {
                print("Error in green function")
            }
        }
    }
    
    
    func showTime() {
        
        print("time....\(self.timeHoursAndMinutes)")
        let parameters = ["\(self.timeHoursAndMinutes)"]
        var task = myPhoton!.callFunction("showTime", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle")
            }
            else {
                print("Error in green function")
            }
        }
    }
    
    
    
    func chandeWeather(weather : String) {
        let parameters = ["\(weather)"]
        var task = myPhoton!.callFunction("changeTheColor", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle")
            }
            else {
                print("Error in green function")
            }
        }
    }
    
    
}
