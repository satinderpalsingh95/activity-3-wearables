# Activity 3 & 4 - Cloudy with a Chance of Meatballs

### Group Members:

-   **Satinder Pal Singh (CO740860)**

-   **Prabhjinder Singh (CO740758)**

-   **Gurlagan Singh (CO734665)**
----------
We used the lights of different colors to display the time, temperature and weather
Colors: Red(255,0,0), Green(17,255,0), Blue(17,33,253), Yellow(255,255,0),              Grey(169,169,169), White(255,255,255), Pink(255,6,255)
1. To display time: time is in the form HH:MM
we use the colours (Red Green : Blue Yellow)
2. To display the temperature
we use the colors (Blue Yellow). Blue for the digit at tens place and yellow for the digit at once place. For the negative temperature all leds blink with white color before telling the temperature.
3. To display the weather:
Weather      -        Color
Clouds       -        Grey
Snow          -       White
Rain and Mist -       Blue
Fog           -       Pink
Haze          -       Red
Sunny and Clear -     Yellow


---------
